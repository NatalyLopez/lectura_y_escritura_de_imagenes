# importar librería OpenCV
import cv2 as cv
# Funciones de Sistema Operativo
import os

initSequence=1 # inicio secuencia
numSequences= 5 # fin secuencia
cont_frame = 0 # contador auxiliar para implementar

#Directorio de lectura
path_RGB = "../DATASETS/17flowers/RGB"

# Directorios de escritura
# Imágenes GRAY
path_GRAY = "./Output-Gray"
# Imágenes BINARIAS (B/N)
path_Binary = "./Output-Binary"

# Canal R (RED)
path_R = "./channel-RED"
# Canal G (GREEN)
path_G = "./channel-GREEN"
# Canal B (BLUE)
path_B = "./channel-BLUE"

totalImages = int(len(os.listdir(path_RGB))) # Contabiliza número de archivos
# Bucle recorre desde el inicio a fin en un rango
cont_frame = 0 # contador auxiliar para implementar
for ns in range(initSequence,numSequences+1):
    cont_frame = cont_frame + 1
    dirimages = path_RGB + '/image_' + str(ns).zfill(4) + '.jpg'
    img = cv.imread(dirimages)
    # Transformar a ESCALA DE GRISES la imagen en color
    gray=cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    (thresh, blackAndWhiteImage) = cv.threshold(gray, 107, 255, cv.THRESH_BINARY)

    cv.imwrite(path_GRAY + '/grayimage_' + str(ns).zfill(4) + '.jpg', gray)
    cv.imwrite(path_Binary + '/binaryimage_' + str(ns).zfill(4) + '.jpg', blackAndWhiteImage)

    # Separa canales RGB
    B = cv.extractChannel(img, 0)
    cv.imwrite(path_B + '/blueimage_' + str(ns).zfill(4) + '.jpg', B)

    G = cv.extractChannel(img, 1)
    cv.imwrite(path_G + '/greenimage_' + str(ns).zfill(4) + '.jpg', G)

    R = cv.extractChannel(img, 2)
    cv.imwrite(path_R + '/redimage_' + str(ns).zfill(4) + '.jpg', R)

print(dirimages)
print(cont_frame)
print(totalImages)